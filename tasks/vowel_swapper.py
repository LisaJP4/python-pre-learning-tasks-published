def vowel_swapper(string):
    # ==============
    # print(vowel_swapper(“a, e, i, o, u”) # Should print “4, 3, !, ooo, |_|” to the console

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
